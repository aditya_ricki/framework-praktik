<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use Spatie\Permission\PermissionRegistrar;

class PermissionsDemoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Reset cached roles and permissions
        app()[PermissionRegistrar::class]->forgetCachedPermissions();

        // create permissions
        // Permission::create(['name' => 'create users']);
        // Permission::create(['name' => 'read users']);
        // Permission::create(['name' => 'edit users']);
        // Permission::create(['name' => 'delete users']);

        // Permission::create(['name' => 'create roles']);
        // Permission::create(['name' => 'read roles']);
        // Permission::create(['name' => 'edit roles']);
        // Permission::create(['name' => 'delete roles']);

        // Permission::create(['name' => 'create menus']);
        // Permission::create(['name' => 'read menus']);
        // Permission::create(['name' => 'edit menus']);
        // Permission::create(['name' => 'delete menus']);

        // Permission::create(['name' => 'create products']);
        // Permission::create(['name' => 'read products']);
        // Permission::create(['name' => 'edit products']);
        // Permission::create(['name' => 'delete products']);

        // create roles and assign existing permissions
        $role1 = Role::create(['name' => 'Pegawai']);
        // $role1->givePermissionTo('read users');
        // $role1->givePermissionTo('read products');

        $role2 = Role::create(['name' => 'Suplier']);
        // $role2->givePermissionTo('read products');

        $role3 = Role::create(['name' => 'Produksi']);
        // $role3->givePermissionTo('read products');

        $role4 = Role::create(['name' => 'Super-Admin']);
        // gets all permissions via Gate::before rule; see AuthServiceProvider

        // create demo users
        $user = \App\Models\User::factory()->create([
            'name' => 'Pegawai',
            'email' => 'adityaric73@gmail.com',
        ]);
        $user->assignRole($role1);

        $user = \App\Models\User::factory()->create([
            'name' => 'Suplier',
            'email' => 'leniseptiani1508@gmail.com',
        ]);
        $user->assignRole($role2);

        $user = \App\Models\User::factory()->create([
            'name' => 'Customer',
            'email' => 'linaseptiana1508@gmail.com',
        ]);
        $user->assignRole($role3);

        $user = \App\Models\User::factory()->create([
            'name' => 'Super Admin',
            'email' => 'adityaric72@gmail.com',
        ]);
        $user->assignRole($role4);
        $user->assignRole($role3);
        $user->assignRole($role2);
        $user->assignRole($role1);
    }
}
