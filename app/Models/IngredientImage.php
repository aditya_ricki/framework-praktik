<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class IngredientImage extends Model
{
    use HasFactory;

    protected $fillable = [
    	'ingredient_id',
    	'path',
    ];

    public function ingredient()
    {
    	return $this->belongsTo(Ingredient::class);
    }
}
