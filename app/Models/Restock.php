<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Restock extends Model
{
    use HasFactory;

    protected $fillable = [
    	'ingredient_id',
    	'quantity',
    	'price',
    ];

    public function ingredient()
    {
        return $this->belongsTo(Ingredient::class);
    }
}
