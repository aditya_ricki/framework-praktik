<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;

    protected $fillable = [
    	'produksi_id',
    	'name',
    	'quantity',
    	'information',
    	'status',
    ];

    public function produksi()
    {
        return $this->belongsTo(User::class, 'produksi_id');
    }

    public function ingredients()
    {
        return $this->belongsToMany(Ingredient::class, 'ingredient_product');
    }
}
