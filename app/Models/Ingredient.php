<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Ingredient extends Model
{
    use HasFactory;

    protected $fillable = [
    	'suplier_id',
    	'category_id',
    	'size_id',
    	'name',
    	'quantity',
    	'price',
    ];

    public function category()
    {
    	return $this->belongsTo(Category::class);
    }

    public function suplier()
    {
        return $this->belongsTo(User::class, 'suplier_id');
    }

    public function size()
    {
        return $this->belongsTo(Size::class);
    }

    public function ingredientImages()
    {
        return $this->hasMany(IngredientImage::class);
    }

    public function products()
    {
        return $this->belongsToMany(Product::class, 'ingredient_product');
    }

    public function restocks()
    {
        return $this->hasMany(Restock::class);
    }
}
