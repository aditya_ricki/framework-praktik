<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    // authenticate users
    protected function authenticated(Request $request, $user)
    {
        // switch ($user) {
        //     case $user->hasRole('Super-Admin'):
        //         return redirect()->route('super-admin.page');
        //         break;
        //     case $user->hasRole('Pegawai'):
        //         return redirect()->route('pegawai.page');
        //         break;
        //     case $user->hasRole('Suplier'):
        //         return redirect()->route('suplier.page');
        //         break;
        //     case $user->hasRole('Customer'):
        //         return redirect()->route('welcome');
        //         break;
        //     default:
        //         return abort(403);
        //         break;
        // }
    }
}
