<?php

namespace App\Http\Controllers\Pegawai;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ingredient;
use App\Models\IngredientImage;
use App\Models\Category;
use App\Models\User;
use App\Models\Size;
use Storage;

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ingredients = Ingredient::with('suplier')
                       ->with('category')
                       ->with('size')
                       ->orderBy('suplier_id', 'desc')
                       ->get();

        return view('pegawai.ingredient.index', ['ingredients' => $ingredients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $supliers   = User::whereHas('roles', function ($query) {
                        $query->where('roles.name', 'Suplier');
                      })->get();
        $sizes      = Size::all();

        return view('pegawai.ingredient.create', [
            'categories' => $categories,
            'supliers'   => $supliers,
            'sizes'      => $sizes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'suplier_id'  => 'required',
            'size_id'     => 'required',
            'name'        => 'required',
            'price'       => 'required',
            'quantity'    => 'required',
        ]);

        Ingredient::create($request->all());

        return redirect()->back()->with('success', 'Ingredient created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $ingredient = Ingredient::find($id);
        $categories = Category::all();
        $supliers   = User::whereHas('roles', function ($query) {
                        $query->where('roles.name', 'Suplier');
                      })->get();
        $sizes      = Size::all();

        return view('pegawai.ingredient.edit', [
            'ingredient' => $ingredient,
            'categories' => $categories,
            'supliers'   => $supliers,
            'sizes'      => $sizes,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'category_id' => 'required',
            'suplier_id'  => 'required',
            'size_id'     => 'required',
            'name'        => 'required',
            'price'       => 'required',
            'quantity'    => 'required',
        ]);

        Ingredient::find($id)->update($request->all());

        return redirect()->back()->with('success', 'Ingredient updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ingredient = Ingredient::find($id);

        $ingredient->ingredientImages->map(function ($image) {
            Storage::delete($image->path);
            $image->delete();
        });

        $ingredient->delete();

        return redirect()->back()->with('success', 'Ingredient successfully removed!');
    }

    // get image items by ingredient
    public function getImageItemsByItem($ingredientId)
    {
        $ingredient = Ingredient::with('ingredientImages')->find($ingredientId);

        return view('pegawai.ingredient.ingredient-image', [
            'ingredient' => $ingredient,
        ]);
    }

    // store image items by item
    public function storeImageItemsByItem(Request $request, $ingredientId)
    {
        $request->validate([
            'image' => 'required|file|max:2000',
        ]);

        $path = Storage::putFile(
                    'public/images',
                    $request->file('image'),
                );

        IngredientImage::create([
            'ingredient_id' => $ingredientId,
            'path'          => $path,
        ]);

        return redirect()->back()->with('success', 'Image item uploaded!');
    }

    public function deleteImageItemsById($imageItem)
    {
        $file = IngredientImage::find($imageItem);

        Storage::delete($file->path);
        $file->delete();

        return redirect()->back()->with('success', 'Image item removed!');
    }
}
