<?php

namespace App\Http\Controllers\Produksi;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Ingredient;
use App\Mail\ProductEmail;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\IngredientProduct;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at')->with('ingredients')->get();

        return view('produksi.product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ingredients = Ingredient::all();

        return view('produksi.product.create', ['ingredients' => $ingredients]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'required',
            'quantity'    => 'required',
            'information' => 'required',
            'ingredients' => 'required|array',
        ]);

        try {
            $product = Product::create([
                'produksi_id' => $request->user()->id,
                'name'        => $request->name,
                'information' => $request->information,
                'quantity'    => $request->quantity,
                'status'      => 'PENDING',
            ]);

            $product->ingredients()->attach($request->ingredients);

            User::whereHas('roles', function ($query) {
                $query->where('roles.name', 'Super-Admin');
            })->get()->map(function ($superadmin) use ($request, $product) {
                $updateUrl = 'http://localhost:8000/super-admin/update-from-email/' . $product->id;

                Mail::to($superadmin->email)->queue(new ProductEmail($superadmin->name, $request->user()->name, $updateUrl, $product->id));
            });

            return redirect()->back()->with('success', 'Product request successfully!');
        } catch (Exception $e) {
            return abort(404, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('produksi.product.show', [
            'product' => $product,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Product $product)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        if ($product->status == 'READY') {
            // kembalikan stok ingredient
            $product->ingredients->map(function ($ingredient) use ($product) {
                $ingredient->update([
                    'quantity' => $ingredient->quantity + $product->quantity,
                ]);
            });
        }

        // hapus row tabel ingredient_product
        $product->ingredients()->detach();

        // delete product
        $product->delete();

        return redirect()->back()->with('success', 'Product delete successfully!');
    }
}
