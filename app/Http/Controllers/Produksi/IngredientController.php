<?php

namespace App\Http\Controllers\Produksi;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ingredient;

class IngredientController extends Controller
{
	public function index()
	{
		$ingredients = Ingredient::with(['suplier', 'category', 'size'])
					   ->get();

		return view('produksi.ingredient.index', [
			'ingredients' => $ingredients,
		]);
	}

	public function image($id)
	{
        $ingredient = Ingredient::with('ingredientImages')->find($id);

        return view('produksi.ingredient.ingredient-image', ['ingredient' => $ingredient]);
	}
}
