<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Restock;
use App\Mail\AcceptRestock;
use Illuminate\Support\Facades\Mail;

class RestockController extends Controller
{
	public function index()
	{
		$restocks = Restock::with('ingredient')->get();

		return view('superadmin.restock.index', [
			'restocks' => $restocks,
		]);
	}

	public function acceptRestock($id)
	{
		$restock = Restock::with('ingredient.suplier')->find($id);

		$restock->status = 'ACCEPT';
		$restock->save();

		$restock->ingredient->update([
			'quantity' => $restock->ingredient->quantity + $restock->quantity,
		]);

		Mail::to($restock->ingredient->suplier->email)->queue(new AcceptRestock($restock->ingredient->suplier->name, $restock->ingredient->name));

		return redirect()->back()->with('success', 'Restock accept!');
	}
}
