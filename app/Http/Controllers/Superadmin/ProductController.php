<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\Ingredient;
use App\Mail\ProductEmail;
use App\Mail\RequestProductToSuplier;
use App\Mail\ResponseProductIsReady;
use Illuminate\Support\Facades\Mail;
use App\Models\User;
use App\Models\IngredientProduct;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::orderBy('created_at')->with('ingredients')->get();

        return view('superadmin.product.index', ['products' => $products]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $ingredients = Ingredient::all();

        return view('superadmin.product.create', ['ingredients' => $ingredients]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name'        => 'required',
            'quantity'    => 'required',
            'information' => 'required',
            'ingredients' => 'required|array',
        ]);

        try {
            $product = Product::create([
                'produksi_id' => $request->user()->id,
                'name'        => $request->name,
                'information' => $request->information,
                'quantity'    => $request->quantity,
                'status'      => 'PENDING',
            ]);

            $product->ingredients()->attach($request->ingredients);

            User::whereHas('roles', function ($query) {
                $query->where('roles.name', 'Super-Admin');
            })
            ->get()
            ->map(function ($superadmin) use ($request, $product) {
                $updateUrl = 'http://localhost:8000/super-admin/update-from-email/' . $product->id;

                Mail::to($superadmin->email)->queue(new ProductEmail($superadmin->name, $request->user()->name, $updateUrl, $product->id));
            });

            return redirect()->back()->with('success', 'Product request successfully!');
        } catch (Exception $e) {
            return abort(404, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::with(['produksi', 'ingredients'])->find($id);

        return view('produksi.product.show', [
            'product' => $product,
        ]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $product = Product::find($id);

        return view('product.edit', ['product' => $product]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        dd($request->all());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // cari produk dan ingredient
        $product = Product::with('ingredients')->find($id);

        if ($product->status == 'READY') {
            // kembalikan stok ingredient
            $product->ingredients->map(function ($ingredient) use ($product) {
                $ingredient->update([
                    'quantity' => $ingredient->quantity + $product->quantity,
                ]);
            });
        }

        // hapus row tabel ingredient_product
        $product->ingredients()->detach();

        // delete product
        $product->delete();

        return redirect()->back()->with('success', 'Product delete successfully!');
    }

    // request product to suplier
    private function requestProductToSuplier($ingredient_id)
    {
        $ingredient = Ingredient::find($ingredient_id);

        Mail::to($ingredient->suplier->email)
        ->queue(new RequestProductToSuplier($ingredient->suplier->name, $ingredient->name));

        return true;
    }

    // kirim email ke bagian produksi
    private function responseProductIsReady($id)
    {
        $product = Product::find($id);

        User::whereHas('roles', function ($query) {
            $query->where('roles.name', 'Produksi');
        })
        ->get()
        ->map(function ($produksi) use ($product) {
            Mail::to($produksi->email)->queue(new ResponseProductIsReady($produksi->name, $product->name));
        });

        return true;
    }

    // update status product
    private function updateStatusProduct($id, $status)
    {
        Product::find($id)
        ->update([
            'status' => $status,
        ]);

        return true;
    }

    // update from email
    public function updateFromEmail(Request $request, $id)
    {
        $this->updateStatusProduct($id, 'INCOMPELETE');

        $readyStock = true;
        $check      = IngredientProduct::with(['product', 'ingredient'])
                      ->where('product_id', $id)
                      ->get()
                      ->map(function ($ip) use ($readyStock) {
                        if ($ip->ingredient->quantity < $ip->product->quantity) {
                            $readyStock = false;
                            $this->requestProductToSuplier($ip->ingredient->id);
                        }

                        return $readyStock;
                      });

        if (!in_array(false, $check->toArray())) {
            $this->updateStatusProduct($id, 'READY');
            $this->responseProductIsReady($id);

            IngredientProduct::with(['product', 'ingredient'])
            ->where('product_id', $id)
            ->get()
            ->map(function ($ip) {
                Ingredient::find($ip->ingredient->id)
                ->update([
                    'quantity' => $ip->ingredient->quantity - $ip->product->quantity,
                ]);
            });

            $message = 'Product is ready for production!';
        } else {
            $message = 'Product is not ready for production!';
        };

        return redirect()->route('superadmin-product.index')->with('success', $message);
    }
}
