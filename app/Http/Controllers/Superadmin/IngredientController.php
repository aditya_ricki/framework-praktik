<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Ingredient;
use App\Models\Category;
use App\Models\User;
use App\Models\Size;
use Illuminate\Http\Request;
use Storage;

class IngredientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ingredients = Ingredient::with('suplier')
                       ->with('category')
                       ->with('size')
                       ->orderBy('suplier_id', 'desc')
                       ->get();

        return view('superadmin.ingredient.index', ['ingredients' => $ingredients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        $supliers   = User::whereHas('roles', function ($query) {
                        $query->where('roles.name', 'Suplier');
                      })->get();
        $sizes      = Size::all();

        return view('superadmin.ingredient.create', [
            'categories' => $categories,
            'supliers'   => $supliers,
            'sizes'      => $sizes,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'category_id' => 'required',
            'suplier_id'  => 'required',
            'size_id'     => 'required',
            'name'        => 'required',
            'price'       => 'required',
            'quantity'    => 'required',
        ]);

        Ingredient::create($request->all());

        return redirect()->back()->with('success', 'Ingredient created!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function show(Ingredient $ingredient)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function edit(Ingredient $ingredient)
    {
        $categories = Category::all();
        $supliers   = User::whereHas('roles', function ($query) {
                        $query->where('roles.name', 'Suplier');
                      })->get();
        $sizes      = Size::all();

        return view('superadmin.ingredient.edit', [
            'ingredient' => $ingredient,
            'categories' => $categories,
            'supliers'   => $supliers,
            'sizes'      => $sizes,
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Ingredient $ingredient)
    {
        $request->validate([
            'category_id' => 'required',
            'suplier_id'  => 'required',
            'size_id'     => 'required',
            'name'        => 'required',
            'price'       => 'required',
            'quantity'    => 'required',
        ]);

        $ingredient->update($request->all());

        return redirect()->back()->with('success', 'Ingredient updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ingredient  $ingredient
     * @return \Illuminate\Http\Response
     */
    public function destroy(Ingredient $ingredient)
    {
        $ingredient->ingredientImages->map(function ($image) {
            Storage::delete($image->path);
            $image->delete();
        });

        $ingredient->delete();

        return redirect()->back()->with('success', 'Ingredient successfully removed!');
    }
}
