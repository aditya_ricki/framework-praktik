<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Product;
use PDF;

class LaporanController extends Controller
{
	public function index()
	{
		return view('superadmin.laporan.index');
	}

	public function generateIngredientTerpakai(Request $request)
	{
		$part = $request->query()['part'];

		if ($part == 'all') {
			$products = Product::with('ingredients.suplier')
			->where('status', 'READY')
			->get();
		}

		$name = 'Laporan-Ingredient-Terpakai-' . date('YmdHis');

		$pdf  = PDF::loadview('superadmin.laporan.ingredient-terpakai', [
			'products' => $products,
			'name'     => $name,
		]);

    	return $pdf->download($name);

    	// return redirect()->back()->with('success', 'Laporan generated!');
	}
}
