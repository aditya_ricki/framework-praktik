<?php

namespace App\Http\Controllers\Superadmin;

use App\Http\Controllers\Controller;
use App\Models\Ingredient;
use App\Models\IngredientImage;
use Illuminate\Http\Request;
use Storage;

class IngredientImageController extends Controller
{
    // get image items by ingredient
    public function getImageItemsByItem($ingredientId)
    {
        $ingredient = Ingredient::with('ingredientImages')->find($ingredientId);

        return view('superadmin.ingredient.ingredient-image', ['ingredient' => $ingredient]);
    }

    // store image items by item
    public function storeImageItemsByItem(Request $request, $ingredientId)
    {
        $request->validate([
            'image' => 'required|file|max:2000',
        ]);

        $path = Storage::putFile(
                    'public/images',
                    $request->file('image'),
                );

        IngredientImage::create([
            'ingredient_id' => $ingredientId,
            'path'          => $path,
        ]);

        return redirect()->back()->with('success', 'Image item uploaded!');
    }

    public function deleteImageItemsById($imageItem)
    {
        $file = IngredientImage::find($imageItem);

        Storage::delete($file->path);
        $file->delete();

        return redirect()->back()->with('success', 'Image item removed!');
    }
}
