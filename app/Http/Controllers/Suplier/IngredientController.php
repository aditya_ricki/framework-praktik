<?php

namespace App\Http\Controllers\Suplier;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Ingredient;

class IngredientController extends Controller
{
	public function index(Request $request)
	{
		$ingredients = Ingredient::with(['category', 'size'])
					   ->where('suplier_id', $request->user()->id)
					   ->get();

		return view('suplier.ingredient.index', [
			'ingredients' => $ingredients,
		]);
	}

	public function image($id)
	{
        $ingredient = Ingredient::with('ingredientImages')->find($id);

        return view('suplier.ingredient.ingredient-image', ['ingredient' => $ingredient]);
	}
}
