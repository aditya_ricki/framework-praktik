<?php

namespace App\Http\Controllers\Suplier;

use App\Http\Controllers\Controller;
use App\Models\Restock;
use Illuminate\Http\Request;
use App\Models\Ingredient;
use App\Models\User;
use App\Mail\RestockIngredient;
use Illuminate\Support\Facades\Mail;

class RestockController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $email    = $request->user()->email;
        $restocks = Restock::with('ingredient.suplier')
                    ->get()
                    ->where('ingredient.suplier.email', $email);

        return view('suplier.restock.index', [
            'restocks' => $restocks,
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        $email       = $request->user()->email;
        $ingredients = Ingredient::with('suplier')->get()->where('suplier.email', $email);

        return view('suplier.restock.create', [
            'ingredients' => $ingredients,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'ingredient_id' => 'required',
            'price'         => 'required',
            'quantity'      => 'required',
        ]);

        try {
            $ingredient = Ingredient::find($request->ingredient_id);

            $restock    = Restock::create([
                'ingredient_id' => $request->ingredient_id,
                'price'         => $request->price,
                'quantity'      => $request->quantity,
            ]);

            User::whereHas('roles', function ($query) {
                $query->where('roles.name', 'Super-Admin');
            })
            ->get()
            ->map(function ($superadmin) use ($restock, $ingredient, $request) {
                $updateUrl = 'http://localhost:8000/super-admin/accept-restock/' . $restock->id;

                Mail::to($superadmin->email)->queue(new RestockIngredient($superadmin->name, $request->user()->name, $updateUrl, $ingredient->name));
            });

            return redirect()->back()->with('success', 'Restock successfully!');
        } catch (Exception $e) {
            return abort(404, $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Restock  $restock
     * @return \Illuminate\Http\Response
     */
    public function show(Restock $restock)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Restock  $restock
     * @return \Illuminate\Http\Response
     */
    public function edit(Restock $restock)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Restock  $restock
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Restock $restock)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Restock  $restock
     * @return \Illuminate\Http\Response
     */
    public function destroy(Restock $restock)
    {
        if ($restock->status == 'ACCEPT') {
            $restock->ingredient->update([
                'quantity' => $restock->ingredient->quantity - $restock->quantity,
            ]);
        }

        $restock->delete();

        return redirect()->back()->with('success', 'Restock deleted!');
    }
}
