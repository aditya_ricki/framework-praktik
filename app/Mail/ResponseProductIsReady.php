<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ResponseProductIsReady extends Mailable
{
    use Queueable, SerializesModels;

    public $produksiName, $productName;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($produksiName, $productName)
    {
        $this->produksiName = $produksiName;
        $this->productName  = $productName;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.response-product-is-ready');
    }
}
