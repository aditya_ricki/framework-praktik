<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class AcceptRestock extends Mailable
{
    use Queueable, SerializesModels;

    public $suplier, $ingredient;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($suplier, $ingredient)
    {
        $this->suplier    = $suplier;
        $this->ingredient = $ingredient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.response-accept-restock');
    }
}
