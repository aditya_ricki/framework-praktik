<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class RestockIngredient extends Mailable
{
    use Queueable, SerializesModels;

    public $admin, $suplier, $updateUrl, $ingredient;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($admin, $suplier, $updateUrl, $ingredient)
    {
        $this->admin      = $admin;
        $this->suplier    = $suplier;
        $this->updateUrl  = $updateUrl;
        $this->ingredient = $ingredient;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.accept-restock');
    }
}
