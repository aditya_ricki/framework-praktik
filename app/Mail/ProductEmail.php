<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ProductEmail extends Mailable
{
    use Queueable, SerializesModels;

    public $admin, $produksi, $updateUrl, $productId;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($admin, $produksi, $updateUrl, $productId)
    {
        $this->admin     = $admin;
        $this->produksi  = $produksi;
        $this->updateUrl = $updateUrl;
        $this->productId = $productId;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('email.product-store');
    }
}
