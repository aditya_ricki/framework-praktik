<?php

use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomeController::class, 'index'])->name('welcome');

Auth::routes(['verify' => true]);

// Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/email/verify', function () {
    return view('auth.verify');
})->middleware('auth')->name('verification.notice');

Route::get('/forgot-password', function () {
    return view('auth.passwords.email');
})->middleware('guest')->name('password.request');









// superadmin
Route::group([
	'middleware' => ['role:Super-Admin', 'auth', 'verified'],
	'prefix'     => 'super-admin',
], function () {
    Route::get('/', [App\Http\Controllers\Superadmin\DashboardController::class, 'index'])->name('super-admin.page');
    Route::resource('role', App\Http\Controllers\Superadmin\RoleController::class);
    Route::resource('user', App\Http\Controllers\Superadmin\UserController::class, [
    	'except' => [
    		'destroy',
    		'show',
    	],
    ]);
    Route::resource('size', App\Http\Controllers\Superadmin\SizeController::class);
    Route::resource('category', App\Http\Controllers\Superadmin\CategoryController::class);
    Route::resource('ingredient', App\Http\Controllers\Superadmin\IngredientController::class);
    Route::resource('product', App\Http\Controllers\Superadmin\ProductController::class, [
		'except' => [
			'edit',
			'update',
		],
		'names'  => [
			'index'   => 'superadmin-product.index',
			'create'  => 'superadmin-product.create',
			'store'   => 'superadmin-product.store',
			'show'    => 'superadmin-product.show',
			'destroy' => 'superadmin-product.destroy',
    	],
    ]);
    Route::get('laporan', [App\Http\Controllers\Superadmin\LaporanController::class, 'index'])->name('superadmin.laporan');
    Route::get('laporan/generate-ingredient-terpakai', [App\Http\Controllers\Superadmin\LaporanController::class, 'generateIngredientTerpakai'])->name('superadmin.laporan-ingredient-terpakai');

	Route::get('ingredient-image/{ingredientId}', [App\Http\Controllers\Superadmin\IngredientImageController::class, 'getImageItemsByItem'])->name('ingredient-image');
	Route::post('ingredient-image/{ingredientId}', [App\Http\Controllers\Superadmin\IngredientImageController::class, 'storeImageItemsByItem'])->name('ingredient-image.store');
	Route::delete('ingredient-image/{ingredientId}', [App\Http\Controllers\Superadmin\IngredientImageController::class, 'deleteImageItemsById'])->name('ingredient-image.destroy');

	Route::get('update-from-email/{productId}', [App\Http\Controllers\Superadmin\ProductController::class, 'updateFromEmail'])->name('superadmin.update-from-email');
	Route::get('restock', [App\Http\Controllers\Superadmin\RestockController::class, 'index'])->name('superadmin-restock.index');
	Route::get('accept-restock/{id}', [App\Http\Controllers\Superadmin\RestockController::class, 'acceptRestock'])->name('accept-restock');
});









// produksi
Route::group([
	'middleware' => ['role:Produksi', 'auth', 'verified'],
	'prefix'     => 'produksi',
], function () {
    Route::get('/', [App\Http\Controllers\Produksi\DashboardController::class, 'index'])->name('produksi.page');
    Route::get('ingredient', [App\Http\Controllers\Produksi\IngredientController::class, 'index'])->name('produksi-ingredient.index');
    Route::get('ingredient-image/{id}', [App\Http\Controllers\Produksi\IngredientController::class, 'image'])->name('produksi-ingredient-image');
    Route::resource('product', App\Http\Controllers\Produksi\ProductController::class, [
		'except' => [
			'edit',
			'update',
		],
		'names'  => [
			'index'   => 'produksi-product.index',
			'create'  => 'produksi-product.create',
			'store'   => 'produksi-product.store',
			'show'    => 'produksi-product.show',
			'destroy' => 'produksi-product.destroy',
    	]
    ]);
});









// pegawai
Route::group([
	'middleware' => ['role:Pegawai', 'auth', 'verified'],
	'prefix'     => 'pegawai',
], function () {
    Route::get('/', [App\Http\Controllers\Pegawai\DashboardController::class, 'index'])->name('pegawai.page');
    Route::resource('suplier', App\Http\Controllers\Pegawai\SuplierController::class, [
    	'except' => [
    		'destroy',
    		'show',
    	],
    ]);
    Route::resource('ingredient', App\Http\Controllers\Pegawai\IngredientController::class, [
		'except' => [
			'show',
		],
		'names'  => [
			'index'   => 'pegawai-ingredient.index',
			'create'  => 'pegawai-ingredient.create',
			'store'   => 'pegawai-ingredient.store',
			'edit'    => 'pegawai-ingredient.edit',
			'update'  => 'pegawai-ingredient.update',
			'destroy' => 'pegawai-ingredient.destroy',
    	]
    ]);

	Route::get('ingredient-image/{id}', [App\Http\Controllers\Pegawai\IngredientController::class, 'getImageItemsByItem'])->name('pegawai-ingredient-image');
	Route::post('ingredient-image/{id}', [App\Http\Controllers\Pegawai\IngredientController::class, 'storeImageItemsByItem'])->name('pegawai-ingredient-image.store');
	Route::delete('ingredient-image/{id}', [App\Http\Controllers\Pegawai\IngredientController::class, 'deleteImageItemsById'])->name('pegawai-ingredient-image.destroy');
});









// suplier
Route::group([
	'middleware' => ['role:Suplier', 'auth', 'verified'],
	'prefix'     => 'suplier',
], function () {
    Route::get('/', [App\Http\Controllers\Suplier\DashboardController::class, 'index'])->name('suplier.page');
    Route::get('ingredient', [App\Http\Controllers\Suplier\IngredientController::class, 'index'])->name('suplier-ingredient.index');
    Route::get('ingredient-image/{id}', [App\Http\Controllers\Suplier\IngredientController::class, 'image'])->name('suplier-ingredient-image');
    Route::resource('/restock', App\Http\Controllers\Suplier\RestockController::class, [
		'except' => [
			'edit',
			'update',
		],
		'names'  => [
			'index'   => 'suplier-restock.index',
			'create'  => 'suplier-restock.create',
			'store'   => 'suplier-restock.store',
			'destroy' => 'suplier-restock.destroy',
    	]
    ]);
});