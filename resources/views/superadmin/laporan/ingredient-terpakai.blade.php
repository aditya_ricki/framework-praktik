<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <title>{{ $name }}</title>
    </head>
    <body>
        <div class="row">
            <div class="col-12">
                <h3 class="h3 text-center">
                    Laporan Ingredient Terpakai
                </h3>
            </div>
        </div>
        <hr>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Suplier</th>
                    <th>Name</th>
                    <th>Category</th>
                    <th>Price</th>
                    <th>Quantity</th>
                    <th>Sub Total</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $total = 0
                @endphp
                @foreach($products as $product)
                    @foreach($product->ingredients as $ingredient)
                    <tr>
                        <td>{{ $ingredient->suplier->email }}</td>
                        <td>{{ $ingredient->name }}</td>
                        <td>{{ $ingredient->category->category }}</td>
                        <td>@currency($ingredient->price)</td>
                        <td>{{ $product->quantity }} {{ $ingredient->size->name }}</td>
                        <td>
                            @currency($ingredient->price * $product->quantity)
                            @php
                                $total += $ingredient->price * $product->quantity;
                            @endphp
                        </td>
                    </tr>
                    @endforeach
                @endforeach
                <tr>
                    <th colspan="5">
                        Total
                    </th>
                    <td>@currency($total)</td>
                </tr>
            </tbody>
        </table>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    </body>
</html>