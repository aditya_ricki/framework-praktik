@extends('adminlte::page')

@section('title', 'Laporan Page')

@section('content_header')
	<div class="row">
		<div class="col-6">
			<h1 class="m-0 text-dark d-inline">Laporan</h1>

            @if(session('success'))
                <div class="alert alert-success alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif

            @if(session('error'))
                <div class="alert alert-danger alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('error') }}</strong>
                </div>
            @endif
		</div>
	</div>
@stop

@section('content')
    <div class="row">
        <div class="col-6">
            <div class="card">
            	<div class="card-header">
            		Laporan Ingredient Terpakai
            	</div>
	            <form method="POST" id="formLaporanIngredientTerpakai">
	            	@csrf
	            	@method('GET')
	                <div class="card-body">
                    	<label>Rentang waktu</label>
                        <div class="form-group clearfix">
                            <div class="icheck-primary d-inline">
                                <input type="radio" id="all" name="waktu" value="all" checked>
                                <label for="all">All</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="icheck-warning d-inline">
                                <input type="radio" id="harian" name="waktu" value="harian">
                                <label for="harian">Hari ini</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="icheck-info d-inline">
                                <input type="radio" id="bulanan" name="waktu" value="bulanan">
                                <label for="bulanan">Bulan ini</label>
                            </div>
                        </div>
                        <div class="form-group clearfix">
                            <div class="icheck-success d-inline">
                                <input type="radio" id="tahunan" name="waktu" value="tahunan">
                                <label for="tahunan">Tahunan ini</label>
                            </div>
                        </div>
	                </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-file"></i> Generate</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
@stop

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script>
    	setAction = (url) => {
    		$('#formLaporanIngredientTerpakai').attr('action', url);
    	}

    	$(document).ready(() => {
    		setAction('http://localhost:8000/super-admin/laporan/generate-ingredient-terpakai?part=all')

    		$('#all').click(() => {
    			setAction('http://localhost:8000/super-admin/laporan/generate-ingredient-terpakai?part=all')
    		})

    		$('#harian').click(() => {
    			setAction('http://localhost:8000/super-admin/laporan/generate-ingredient-terpakai?part=harian')
    		})

    		$('#bulanan').click(() => {
    			setAction('http://localhost:8000/super-admin/laporan/generate-ingredient-terpakai?part=bulanan')
    		})

    		$('#tahunan').click(() => {
    			setAction('http://localhost:8000/super-admin/laporan/generate-ingredient-terpakai?part=tahunan')
    		})
    	})
    </script>
@stop