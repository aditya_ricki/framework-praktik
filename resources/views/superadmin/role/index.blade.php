@extends('adminlte::page')

@section('title', 'Role Page | Super Admin')

@section('content_header')
    <h1 class="m-0 text-dark d-inline">List of roles</h1>
    <a href="#" class="btn btn-success btn-sm float-right"><i class="fas fa-plus mr-2"></i> Create</a>
@stop

@section('content')
    @foreach($roles as $role)
    <div class="row">
        <div class="col-12">
            <div class="card collapsed-card">
                <div class="card-header">
                    <h3 class="card-title">{{ $role->name }}</h3>

                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-plus"></i></button>
                    </div>
                </div>
                <div class="card-body">
                    @if($role->name != 'Super-Admin')
                        <form action="{{ route('role.update', $role->id) }}">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                @foreach($permissions as $permission)
                                <div class="col-md-3 col-sm-6">
                                    <div class="form-group">
                                        <div class="custom-control custom-switch @if(Str::contains($permission->name, 'delete')) custom-switch-on-danger @elseif(Str::contains($permission->name, 'create')) custom-switch-on-success @elseif(Str::contains($permission->name, 'edit')) custom-switch-on-warning @endif">
                                            <input type="checkbox" class="custom-control-input" id="{{ $permission->id }}" name="permissions[]" {{ $role->hasPermissionTo($permission->name) ? 'checked' : '' }}>
                                            <label class="custom-control-label" for="{{ $permission->id }}">{{ $permission->name }}</label>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            </div>
                            <hr>
                            <div class="row">
                                <button type="submit" class="btn btn-primary btn-sm"><i class="fas fa-save"></i> Update</button>
                            </div>
                        </form>
                    @else
                        <div class="row">
                            @foreach($permissions as $permission)
                            <div class="col-md-3 col-sm-6">
                                <div class="form-group">
                                    <div class="custom-control custom-switch @if(Str::contains($permission->name, 'delete')) custom-switch-on-danger @elseif(Str::contains($permission->name, 'create')) custom-switch-on-success @elseif(Str::contains($permission->name, 'edit')) custom-switch-on-warning @endif">
                                        <input type="checkbox" class="custom-control-input" id="{{ $permission->id }}" name="permissions[]" checked disabled>
                                        <label class="custom-control-label" for="{{ $permission->id }}">{{ $permission->name }}</label>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    @endforeach
@stop
