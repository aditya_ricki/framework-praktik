@extends('adminlte::page')

@section('title', 'Edit Item')

@section('content_header')
    <div class="row">
        <div class="col-md-8">
            <h1 class="m-0 text-dark d-inline">Edit ingredient</h1>
            <a href="{{ route('ingredient.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>

            @if(session('success'))
                <div class="alert alert-success alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <script type="text/javascript">
        function pilihIdCategory(obj, categoryId)
        {
            $('#categoryId').val(categoryId);
            var categoryId = `${$(obj).parent().parent().find("td:eq(1)").text()}`;
            $("#pilihCategory").val(categoryId);
            $(".modal").modal('hide');
        }
    </script>
    <div class="modal fade" id="modalCategory">
        <div class="modal-dialog">
            <div style="width:700px;" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Daftar Category</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="card-body">
                        <table class="table table-striped dataTable">
                        <thead>
                            <th>No</th>
                            <th>Category</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($categories as $category)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $category->category }}</td>
                                    <td>
                                        <button data-dismiss="modal" aria-expanded="false" class="btn btn-primary btn-sm" type="button" onclick="pilihIdCategory(this, <?php echo $category->id ?>)">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <small>&copy 2020 <a href="https://www.msdcode.my.id">Kelompok 4</a> </small>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal-Category -->

    <script type="text/javascript">
        function pilihIdSuplier(obj, suplierId)
        {
            $('#suplierId').val(suplierId);
            var suplierId = `${$(obj).parent().parent().find("td:eq(2)").text()}`;
            $("#pilihSuplier").val(suplierId);
            $(".modal").modal('hide');
        }
    </script>
    <div class="modal fade" id="modalSuplier">
        <div class="modal-dialog">
            <div style="width:700px;" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Daftar Suplier</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="card-body">
                        <table class="table table-striped dataTable">
                        <thead>
                            <th>No</th>
                            <th>Name</th>
                            <th>E-Mail</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($supliers as $suplier)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $suplier->name }}</td>
                                    <td>{{ $suplier->email }}</td>
                                    <td>
                                        <button data-dismiss="modal" aria-expanded="false" class="btn btn-primary btn-sm" type="button" onclick="pilihIdSuplier(this, <?php echo $suplier->id ?>)">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <small>&copy 2020 <a href="https://www.msdcode.my.id">Kelompok 4</a> </small>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal-Suplier -->

    <script type="text/javascript">
        function pilihIdSize(obj, sizeId)
        {
            $('#sizeId').val(sizeId);
            var sizeId = `${$(obj).parent().parent().find("td:eq(1)").text()}`;
            $("#pilihSize").val(sizeId);
            $(".modal").modal('hide');
        }
    </script>
    <div class="modal fade" id="modalSize">
        <div class="modal-dialog">
            <div style="width:700px;" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Daftar Size</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="card-body">
                        <table class="table table-striped dataTable">
                        <thead>
                            <th>No</th>
                            <th>Size</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($sizes as $size)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $size->name }}</td>
                                    <td>
                                        <button data-dismiss="modal" aria-expanded="false" class="btn btn-primary btn-sm" type="button" onclick="pilihIdSize(this, <?php echo $size->id ?>)">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <small>&copy 2020 <a href="https://www.msdcode.my.id">Kelompok 4</a> </small>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal-Size -->

    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Edit Ingredient</h5>
                </div>
                <form method="post" action="{{ route('ingredient.update', ['ingredient' => $ingredient->id]) }}" autocomplete="off">
                    <div class="card-body">
                        @csrf
                        @method('PUT')

                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="category_id">Category</label>
                                    <input type="text" data-toggle="modal" data-target="#modalCategory" name="category_id" class="form-control @error('category_id') is-invalid @enderror" id="pilihCategory" placeholder="Category" readonly value="{{ $ingredient->category->category }}">
                                    <input type="hidden" name="category_id" class="form-control" id="categoryId" readonly value="{{ $ingredient->category_id }}">
                                    @error('category_id')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="suplier_id">Suplier</label>
                                    <input type="text" data-toggle="modal" data-target="#modalSuplier" name="suplier_id" class="form-control @error('suplier_id') is-invalid @enderror" id="pilihSuplier" placeholder="Suplier" readonly value="{{ $ingredient->suplier->email }}">
                                    <input type="hidden" name="suplier_id" class="form-control" id="suplierId" readonly value="{{ $ingredient->suplier_id }}">
                                    @error('category_id')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>

                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="size_id">Size</label>
                                    <input type="text" data-toggle="modal" data-target="#modalSize" name="size_id" class="form-control @error('size_id') is-invalid @enderror" id="pilihSize" placeholder="Size" readonly value="{{ $ingredient->size->name }}">
                                    <input type="hidden" name="size_id" class="form-control" id="sizeId" readonly value="{{ $ingredient->size_id }}">
                                    @error('size_id')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ $ingredient->name }}">
                            @error('name')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                    <label>Price</label>
                                    <input type="number" name="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" placeholder="Price" value="{{ $ingredient->price }}">
                                    @error('price')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('quantity') ? ' has-danger' : '' }}">
                                    <label>Quantity</label>
                                    <input type="number" name="quantity" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" placeholder="Quantity" value="{{ $ingredient->quantity }}">
                                    @error('quantity')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-edit"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
