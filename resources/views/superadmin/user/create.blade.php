@extends('adminlte::page')

@section('title', 'Create User')

@section('content_header')
    <div class="row">
        <div class="col-md-6">
            <h1 class="m-0 text-dark d-inline">Create user</h1>
            <a href="{{ route('user.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>

            @if(session('success'))
                <div class="alert alert-success alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <form method="post" action="{{ route('user.store') }}" autocomplete="off">
                    <div class="card-body">
                        @csrf
                        @method('POST')

                        <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
                            <label>Name</label>
                            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}">
                            @error('name')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <label>E-Mail</label>
                            <input type="email" name="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}">
                            @error('email')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-danger' : '' }}">
                            <label>Password</label>
                            <input type="password" name="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}">
                            @error('password')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-danger' : '' }}">
                            <label>Password confirmation</label>
                            <input type="password" name="password_confirmation" class="form-control{{ $errors->has('password_confirmation') ? ' is-invalid' : '' }}">
                            @error('password_confirmation')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="col-6">
                            <label>Role</label>
                            @error('roles')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                            @foreach($roles as $role)
                            <div class="form-group clearfix{{ $errors->has('roles') ? ' has-danger' : '' }}">
                                <div class="@if($role->name == 'Super-Admin') icheck-primary @elseif($role->name == 'Pegawai') icheck-success @elseif($role->name == 'Produksi') icheck-warning @else icheck-danger @endif d-inline">
                                    <input type="radio" id="{{ $role->id }}" name="roles" value="{{ $role->name }}" @if(old('roles') == $role->name) checked @elseif($role->name == 'Pegawai') checked @endif>
                                    <label for="{{ $role->id }}">
                                        {{ $role->name }}
                                    </label>
                                </div>
                            </div>
                            @endforeach
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-save"></i> Create</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
