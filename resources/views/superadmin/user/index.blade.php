@extends('adminlte::page')

@section('title', 'Users Page')

@section('content_header')
	<div class="row">
		<div class="col-12">
			<h1 class="m-0 text-dark d-inline">List of users</h1>
			<a href="{{ route('user.create') }}" class="btn btn-success float-right"><i class="fas fa-plus"></i> Create</a>
		</div>
	</div>

    @if(session('success'))
        <div class="alert alert-success alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('error') }}</strong>
        </div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="dataTableUser" class="table table-striped table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Role</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($users as $user)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $user->name }}</td>
                                <td>{{ $user->email }}</td>
                                <td>
                                    @foreach($user->roles as $role)
                                    <span class="badge @if($role->name == 'Super-Admin') bg-primary @elseif($role->name == 'Pegawai') bg-success @elseif($role->name == 'Suplier') bg-danger @else bg-warning text-white @endif">
                                        {{ $role->name }}
                                    </span>
                                    @endforeach
                                </td>
                                <td>
                                    {{-- <form action="{{ route('user.destroy', $user->id) }}" method="post" class="d-inline"> --}}
                                        <div class="btn-group">
                                            <a href="{{ route('user.edit', $user->id) }}" class="btn btn-warning btn-sm">
                                                <i class="fas fa-edit text-white"></i>
                                            </a>
                                            {{-- @csrf --}}
                                            {{-- @method('DELETE') --}}
                                            {{-- <button class="btn btn-sm btn-danger"> --}}
                                                {{-- <i class="fas fa-trash text-white"></i> --}}
                                            {{-- </button> --}}
                                        </div>
                                    </form>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script>
        $(document).ready(() => {
            $('#dataTableUser').DataTable();
        })
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
@stop