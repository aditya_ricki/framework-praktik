@extends('adminlte::page')

@section('title', 'Restock Page')

@section('content_header')
    <h1 class="m-0 text-dark d-inline">List of restocks</h1>
    @if(session('success'))
        <div class="alert alert-success alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('error') }}</strong>
        </div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="dataTable" class="table table-striped table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Price</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($restocks as $restock)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $restock->ingredient->name }}</td>
                                <td>{{ $restock->quantity }}</td>
                                <td>@currency($restock->price)</td>
                                <td>{{ $restock->status }}</td>
                                <td>
                                    @if($restock->status != 'ACCEPT')
                                        <a href="{{ route('accept-restock', $restock->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-edit text-white"></i></a>
                                    @else
                                        <button class="btn btn-success btn-sm" disabled><i class="fas fa-check"></i></button>
                                    @endif
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script>
        $(document).ready(() => {
            $('#dataTable').DataTable();
        })
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
@stop