@extends('adminlte::page')

@section('title', 'Edit Category')

@section('content_header')
    <div class="row">
        <div class="col-md-6">
            <h1 class="m-0 text-dark d-inline">Edit category</h1>
            <a href="{{ route('category.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>

            @if(session('success'))
                <div class="alert alert-success alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <div class="card-header">
                    <h5 class="title">Edit Category</h5>
                </div>
                <form method="post" action="{{ route('category.update', ['category' => $category->id]) }}" autocomplete="off">
                    <div class="card-body">
                        @csrf
                        @method('PUT')

                        <div class="form-group{{ $errors->has('category') ? ' has-danger' : '' }}">
                            <label>Code</label>
                            <input type="text" name="category" class="form-control{{ $errors->has('category') ? ' is-invalid' : '' }}" placeholder="Code" value="{{ $category->category }}">
                        </div>
                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-edit"></i> Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
