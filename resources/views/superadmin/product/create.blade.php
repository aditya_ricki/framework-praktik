@extends('adminlte::page')

@section('title', 'Create Product')

@section('content_header')
    <div class="row">
        <div class="col-md-12">
            <h1 class="m-0 text-dark d-inline">Create product</h1>
            <a href="{{ route('superadmin-product.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>

            @if(session('success'))
                <div class="alert alert-success alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <form method="post" action="{{ route('superadmin-product.store') }}" autocomplete="off">
                	<div class="card-header">
                		<div class="row">
                			<div class="col-md-6">Product detail</div>
                			<div class="col-md-6">Product ingredient</div>
                		</div>
                	</div>
                    <div class="card-body">
                        @csrf
                        @method('POST')
                        <div class="row">
	                    	<div class="col-6">
	                    		<div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
		                            <label>Name</label>
		                            <input type="text" name="name" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" placeholder="Name" value="{{ old('name') }}">
		                            @error('name')
		                                <p class="small text-danger">{{ $message }}</p>
		                            @enderror
		                        </div>

	                    		<div class="form-group{{ $errors->has('quantity') ? ' has-danger' : '' }}">
		                            <label>Quantity</label>
		                            <input type="number" name="quantity" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" placeholder="Quantity" value="{{ old('quantity') }}">
		                            @error('quantity')
		                                <p class="small text-danger">{{ $message }}</p>
		                            @enderror
		                        </div>

	                    		<div class="form-group{{ $errors->has('information') ? ' has-danger' : '' }}">
		                            <label>Information</label>
		                            <textarea name="information" rows="3" cols="50" class="form-control{{ $errors->has('information') ? ' is-invalid' : '' }}" placeholder="Information">{{{ old('information') }}}</textarea>
		                            @error('information')
		                                <p class="small text-danger">{{ $message }}</p>
		                            @enderror
		                        </div>
	                    	</div>
	                    	<div class="col-6">
	                            @error('ingredients')
	                                <p class="small text-danger">{{ $message }}</p>
	                            @enderror
	                    		@foreach($ingredients as $ingredient)
	                    		<div class="form-group clearfix{{ $errors->has('ingredients') ? ' has-danger' : '' }}">
									<div class="icheck-primary d-inline">
										<input type="checkbox" id="{{ $ingredient->id }}" name="ingredients[]" value="{{ $ingredient->id }}" {{ is_array(old('ingredients')) && in_array($ingredient->id, old('ingredients')) ? 'checked' : '' }}>
										<label for="{{ $ingredient->id }}">
											{{ $ingredient->name }} | {{ $ingredient->category->category }} | Stock {{ $ingredient->quantity }} {{ $ingredient->size->name }} | @currency($ingredient->price)
										</label>
									</div>
								</div>
								@endforeach
	                    	</div>
	                    </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/icheck-bootstrap/3.0.1/icheck-bootstrap.min.css" integrity="sha512-8vq2g5nHE062j3xor4XxPeZiPjmRDh6wlufQlfC6pdQ/9urJkU07NM0tEREeymP++NczacJ/Q59ul+/K2eYvcg==" crossorigin="anonymous" />
@stop

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
@stop
