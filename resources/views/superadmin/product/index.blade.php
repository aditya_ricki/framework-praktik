@extends('adminlte::page')

@section('title', 'Products Page')

@section('content_header')
    <h1 class="m-0 text-dark d-inline">List of products</h1>
    <a href="{{ route('superadmin-product.create') }}" class="btn btn-success float-right"><i class="fas fa-plus"></i> Create</a>

    @if(session('success'))
        <div class="alert alert-success alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('error') }}</strong>
        </div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <table id="dataTableItem" class="table table-striped table-hover" style="width:100%">
                        <thead>
                            <tr>
                                <th>No</th>
                                <th>Name</th>
                                <th>Qty</th>
                                <th>Info</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($products as $product)
                            <tr>
                                <td>{{ $loop->iteration }}</td>
                                <td>{{ $product->name }}</td>
                                <td>{{ $product->quantity }}</td>
                                <td>{{ $product->information }}</td>
                                <td>{{ $product->status }}</td>
                                <td>
                                    <div class="btn-group">
                                        <form action="{{ route('superadmin-product.destroy', $product->id) }}" method="post" class="d-inline">
                                            @if($product->status != 'READY')
                                                <a href="{{ route('superadmin.update-from-email', $product->id) }}" class="btn btn-warning btn-sm"><i class="fas fa-edit text-white"></i></a>
                                            @else
                                                <button class="btn btn-success btn-sm" disabled><i class="fas fa-check"></i></button>
                                            @endif
                                            <a href="{{ route('superadmin-product.show', $product->id) }}" class="btn btn-info btn-sm"><i class="fas fa-eye text-white"></i></a>
                                            @csrf
                                            @method('DELETE')
                                            <button type="submit" class="btn btn-danger btn-sm">
                                                <i class="fas fa-trash text-white"></i>
                                            </button>
                                        </form>
                                    </div>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script>
        $(document).ready(() => {
            $('#dataTableItem').DataTable();
        })
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
@stop