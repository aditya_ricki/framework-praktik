<!DOCTYPE html>
<html>
<head>
	<title>Email Verification</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom-auth.css') }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
	<div class="infinity-container">
		<div class="logo">
			<img src="{{ asset('custom-image-auth/logo.png') }}" width="150px">
		</div>
		<div class="infinity-form-block">
			<div class="infinity-click-box text-center">Email Verification</div>
			<div class="infinity-fold">
				<div class="infinity-form">
					<div class="reset-confirmation px-3">
					    <div class="mb-4">
					        <h4 class="mb-3">Link was sent</h4>
					        <h6 style="color: #777">Please, check your inbox for a verification.</h6>
					    </div>
						<div class="text-right">
							<form method="POST" action="{{ route('verification.resend') }}">
						        @csrf
						        <button type="submit" class="btn">
						            Resend
						        </button>.
						    </form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>
