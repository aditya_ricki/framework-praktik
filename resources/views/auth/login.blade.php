<!DOCTYPE html>
<html>

<head>
	<title>Login</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom-auth.css') }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
	<div class="infinity-container">
		<!-- Company Logo -->
		<div class="logo">
			<img src="{{ asset('custom-image-auth/logo.png') }}" width="150px">
		</div>

		<!-- FORM CONTAINER BEGIN -->
		<div class="infinity-form-block">
			<div class="infinity-click-box text-center">Login into your account</div>

			<div class="infinity-fold">
				<div class="infinity-form">
		            @if($errors->has('email'))
		                <strong>{{ $errors->first('email') }}</strong>
		            @endif
					<form class="form-box" action="{{ route('login') }}" method="POST">
						@csrf
						<!-- Input Box -->
						<div class="form-input">
							<span><i class="fa fa-envelope"></i></span>
							<input type="email" name="email" placeholder="Email Address" tabindex="10" required value="{{ old('email') }}">
						</div>
						<div class="form-input">
							<span><i class="fa fa-lock"></i></span>
							<input type="password" name="password" placeholder="Password" required>
						</div>
						<div class="row mb-2">
							<!-- Forget Password -->
							<div class="col-6 d-flex align-items-center text-sm">
								<a href="{{ route('password.request') }}" class="forget-link">Forgot password?</a>
							</div>
							<!-- Login Button -->
							<div class="col-6 text-right text-sm">
								<button type="submit" class="btn mb-3">Login</button>
							</div>
						</div>
						<div class="text-center">Don't have an account?
							<a class="register-link" href="{{ route('register' )}}">Register here</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- FORM CONTAINER END -->
	</div>

</body>

</html>
