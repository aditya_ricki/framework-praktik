<!DOCTYPE html>
<html>
<head>
	<title>Reset Password</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" type="text/css" href="{{ asset('css/custom-auth.css') }}">
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
	<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
</head>
<body>
	<div class="infinity-container">
		<!-- Company Logo -->
		<div class="logo">
			<img src="{{ asset('custom-image-auth/logo.png') }}" width="150px">
		</div>
		<!-- FORM CONTAINER BEGIN -->
		<div class="infinity-form-block">
			<div class="infinity-click-box text-center">Reset Password</div>
			<div class="infinity-fold">
				<div class="infinity-form">
					<form class="form-box" action="{{ url('password/reset') }}" method="post">
						@csrf
						<input type="hidden" name="token" value="{{ $token }}">
						<!-- Input Box -->
						<div class="form-input">
							<span><i class="fa fa-envelope"></i></span>
							<input type="email" name="email" placeholder="Email Address" tabindex="10"required>
						</div>
						<div class="form-input">
							<span><i class="fa fa-lock"></i></span>
							<input type="password" name="password" placeholder="Password" required>
						</div>
						<div class="form-input">
							<span><i class="fa fa-lock"></i></span>
							<input type="password" name="password_confirmation" placeholder="Password Confirmation" required>
						</div>
						<!-- Register Button -->
						<div class="col-12 px-0 text-right">
							<button type="submit" class="btn mb-3">Reset</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		<!-- FORM CONTAINER END -->
	</div>
</body>
</html>
