@extends('adminlte::page')

@section('title', 'Show Products Page')

@section('content_header')
    <h1 class="m-0 text-dark d-inline">Show {{ $product->name }}</h1>
    <a href="{{ route('produksi-product.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>
@stop

@section('content')
    <div class="row">
        <div class="col-6">
            <div class="card">
            	<div class="card-header">
            		Detail product
            	</div>
                <div class="card-body">
                	<table class="table table-hover table-striped">
                		<tr>
                			<th>Name</th>
                			<td>:</td>
                			<td>{{ $product->name }}</td>
                		</tr>
                		<tr>
                			<th>Quantity</th>
                			<td>:</td>
                			<td>{{ $product->quantity }}</td>
                		</tr>
                		<tr>
                			<th>Info</th>
                			<td>:</td>
                			<td>{{ $product->information }}</td>
                		</tr>
                		<tr>
                			<th>Status</th>
                			<td>:</td>
                			<td>{{ $product->status }}</td>
                		</tr>
                		<tr>
                			<th>Request from</th>
                			<td>:</td>
                			<td>{{ $product->produksi->name }}</td>
                		</tr>
                	</table>
                </div>
            </div>
        </div>
        <div class="col-6">
            <div class="card">
            	<div class="card-header">
            		List of ingredient
            	</div>
	            <div class="card-body">
	            	<div class="table-responsive">
		            	<table id="dataTableItem" class="table table-hover table-striped">
		            		<thead>
		                        <tr>
		                            <th>No</th>
		                            <th>Name</th>
		                            <th>Category</th>
		                            <th>Price</th>
		                            <th>Quantity</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    	@foreach($product->ingredients as $ingredient)
		                    	<tr>
			                        <td>{{ $loop->iteration }}</td>
			                        <td>{{ $ingredient->name }}</td>
			                        <td>{{ $ingredient->category->category }}</td>
			                        <td>@currency($ingredient->price)</td>
			                        <td>{{ $ingredient->quantity }} {{ $ingredient->size->name }}</td>
			                    </tr>
		                    	@endforeach
		                    </tbody>
		            	</table>
		            </div>
	            </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script>
        $(document).ready(() => {
            $('#dataTableItem').DataTable();
        })
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
@stop