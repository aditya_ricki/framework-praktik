@extends('adminlte::page')

@section('title', 'Ingredient Image Page')

@section('content_header')
    <h1 class="m-0 text-dark d-inline">List of image {{ $ingredient->name }}</h1>
    <a href="{{ route('suplier-ingredient.index') }}" class="btn btn-secondary float-right ml-2"><i class="fas fa-arrow-left"></i> Back</a>

    @if(session('success'))
        <div class="alert alert-success alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('success') }}</strong>
        </div>
    @endif

    @if(session('error'))
        <div class="alert alert-danger alert-block mt-4">
            <button type="button" class="close" data-dismiss="alert">×</button>
            <strong>{{ session('error') }}</strong>
        </div>
    @endif
@stop

@section('content')
    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    @if(count($ingredient->ingredientImages) < 1)
                    <div class="row">
                        <div class="col-md-12 text-center">
                            <p class="text-mute">Image items is empty!</p>
                        </div>
                    </div>
                    @else
                    <div class="row">
                        @foreach($ingredient->ingredientImages as $ingredientImage)
                        <div class="col-sm-2">
                            <img src="{{ Storage::url($ingredientImage->path) }}" class="img-fluid mb-2" alt="#"/>
                        </div>
                        @endforeach
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
@stop

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="{{ asset('vendor/ekko-lightbox/ekko-lightbox.css') }}">
@stop

@section('js')
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="{{ asset('vendor/ekko-lightbox/ekko-lightbox.min.js') }}"></script>
    <script>
        $(document).ready(() => {
            $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                event.preventDefault()
                $(this).ekkoLightbox({
                    alwaysShowClose: true
                })
            })
        })
    </script>
@stop