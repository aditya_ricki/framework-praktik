@extends('adminlte::page')

@section('title', 'Create Restock')

@section('content_header')
    <div class="row">
        <div class="col-md-8">
            <h1 class="m-0 text-dark d-inline">Create restock</h1>
            <a href="{{ route('suplier-restock.index') }}" class="btn btn-secondary float-right"><i class="fas fa-arrow-left"></i> Back</a>

            @if(session('success'))
                <div class="alert alert-success alert-block mt-4">
                    <button type="button" class="close" data-dismiss="alert">×</button>
                    <strong>{{ session('success') }}</strong>
                </div>
            @endif
        </div>
    </div>
@stop

@section('content')
    <script type="text/javascript">
        function pilihIdIngredient(obj, ingredientId)
        {
            $('#ingredientId').val(ingredientId);
            var ingredientId = `${$(obj).parent().parent().find("td:eq(1)").text()}`;
            $("#pilihIngredient").val(ingredientId);
            $(".modal").modal('hide');
        }
    </script>
    <div class="modal fade" id="modalIngredient">
        <div class="modal-dialog">
            <div style="width:700px;" class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Daftar Ingredient</h4>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

                <div class="card-body">
                        <table class="table table-striped dataTable">
                        <thead>
                            <th>No</th>
                            <th>Ingredient</th>
                            <th>Action</th>
                        </thead>
                        <tbody>
                            @foreach ($ingredients as $ingredient)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $ingredient->name }}</td>
                                    <td>
                                        <button data-dismiss="modal" aria-expanded="false" class="btn btn-primary btn-sm" type="button" onclick="pilihIdIngredient(this, <?php echo $ingredient->id ?>)">Pilih</button>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <div class="modal-footer">
                    <small>&copy 2020 <a href="https://www.msdcode.my.id">Kelompok 4</a> </small>
                </div>
            </div>
        </div>
    </div>
    <!-- /.modal-Ingredient -->

    <div class="row">
        <div class="col-md-6">
            <div class="card">
                <form method="post" action="{{ route('suplier-restock.store') }}" autocomplete="off">
                    <div class="card-body">
                        @csrf
                        @method('POST')

                        <div class="form-group">
                            <label for="ingredient_id">Ingredient</label>
                            <input type="text" data-toggle="modal" data-target="#modalIngredient" name="ingredient_name" class="form-control @error('ingredient_id') is-invalid @enderror" id="pilihIngredient" placeholder="Ingredient" readonly>
                            <input type="hidden" name="ingredient_id" class="form-control" id="ingredientId" readonly value="{{ old('ingredient_id') }}">
                            @error('ingredient_id')
                                <p class="small text-danger">{{ $message }}</p>
                            @enderror
                        </div>

                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('price') ? ' has-danger' : '' }}">
                                    <label>Price</label>
                                    <input type="number" name="price" class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" placeholder="Price" value="{{ old('price') }}">
                                    @error('price')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group{{ $errors->has('quantity') ? ' has-danger' : '' }}">
                                    <label>Quantity</label>
                                    <input type="number" name="quantity" class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" placeholder="Stock" value="{{ old('quantity') }}">
                                    @error('quantity')
                                        <p class="small text-danger">{{ $message }}</p>
                                    @enderror
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="card-footer">
                        <button type="submit" class="btn btn-fill btn-primary"><i class="fas fa-save"></i> Save</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection

@section('css')
    <!-- <link rel="stylesheet" href="/css/admin_custom.css"> -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap4.min.css">
@stop

@section('js')
    <script>
        $(document).ready(() => {
            $('.dataTable').DataTable();
        })
    </script>
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap4.min.js"></script>
@stop
